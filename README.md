A simple terminal application to retrieve the population number of world countries consuming the public [World Population API ](http://api.population.io/).

To use it you will need [Node.js](https://nodejs.org/en/) installed.

First download the library dependencies (once): 

```$ npm install```

Then ask for countries' populations: 

```$ node src/index.js country_1|"country_1" ... country_N|"country_N"```  
(You must use quotation marks with country names composed by multiple words)

Example: 

```$ node src/index.js  "United Kingdom" Italy Germany```

There is also a commmand for help: 

```$ node src/index.js --help```

*Have fun!*

### Geek notes
- The application use [axios](https://github.com/axios/axios) for retrieving data from API. 
- Multiple countries requests are executed in parallel using [Promise.all](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all). 
- A test suite is provided using [mocha](https://mochajs.org/), [chai](http://www.chaijs.com/) and [moxios](https://github.com/axios/moxios). 
