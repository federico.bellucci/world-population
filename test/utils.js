const {assert, expect} = require('chai')

const {sortCountries, printCountries} = require('../src/utils')

describe('sortCountries', () => {
    it('should return a sorted array', () => {
        let countries = [
            {
                country: 'France',
                population: 65
            },
            {
                country: 'Italy',
                population: 59
            }, 
            {
                country: 'United Kingdom',
                population: 65
            }, 
            {
                country: 'United States',
                population: 329
            }
        ]
    	let result = sortCountries(countries) 
        expect(result[0].country).to.be.equal('United States')
        expect(result[1].country).to.be.oneOf(['France', 'United Kingdom'])
        expect(result[2].country).to.be.oneOf(['France', 'United Kingdom'])
        expect(result[3].country).to.be.equal('Italy')
    })
})

describe('printCountries', () => {
    it('should string representing the param array', () => {
        let countries = [
            {
                country: 'United Kingdom',
                population: 65
            }, 
            {
                country: 'Italy',
                population: 59
            },    
        ]
        let result = printCountries(countries) 
        expect(result).to.be.a('string').that.contains('United Kingdom')
    })
})