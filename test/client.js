const {assert, expect} = require('chai')
const axios = require('axios')
const moxios = require('moxios')

const {getCountries} = require('../src/client')

describe('getCountries', () => {
    beforeEach(function () {
        moxios.install()
    })

    afterEach(function () {
        moxios.uninstall()
    })

    const data = [
            {
                regex: /United Kingdom/,
                name: 'United Kingdom',
                population: 65
            },
            {
                regex: /Italy/,
                name: 'Italy',
                population: 59
            }
        ],
        today = '2018-08-05';

    it('should return an array of countries and populations', async () => {

        data.forEach(item => {
            moxios.stubRequest(item.regex, {
                status: 200,
                response: {
                    total_population: {
                        date: today,
                        population: item.population
                    }
                }
            })
        })

        let expected = data.map(({name, population}) => ({
            country: name,
            population
        }))
        
        let result = await getCountries({
            countries: data.map(item => item.name), 
            date: today
        })

        expect(result).to.be.an('array').that.have.lengthOf(2)
        expect(result).to.have.deep.members(expected) 
        
    })

    it('should return error if one country name is rejected by API', async () => {

        moxios.stubRequest(data[0].regex, {
            status: 200,
            response: {
                total_population: {
                    date: today,
                    population: data[0].population
                }
            }
        })
        moxios.stubRequest(data[1].regex, {
            status: 400,
            response: { 
                detail: 'Italy is an invalid value for the parameter "country"...'
            }
        })
        
        try {
            await getCountries({
                countries: data.map(item => item.name), 
                date: today
            })
        } catch (error) {
            expect(error).to.be.a('string').that.include('\'Italy\' is an invalid country')
        }
    })

    it('should return a general on error codes different than 400', async () => {
        
        moxios.stubRequest(data[0].regex, {
            status: 200,
            response: {
                total_population: {
                    date: today,
                    population: data[0].population
                }
            }
        })

        moxios.stubRequest(data[1].regex, {
            status: 500
        })

        try {
            await getCountries({
                countries: data.map(item => item.name), 
                date: today
            })
        } catch (error) {
            expect(error).to.be.a('string').that.include('Server error')
        }

    })
})