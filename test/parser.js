// const assert = require('assert')
const {assert, expect} = require('chai')

// const {getCountries} = require('./client')
// const {sortCountries, printCountries} = require('./utils')
const {parseParams} = require('../src/parser')

describe('parseParams', () => {
    it('should return error with no parameters', async () => {
        let params = []
        try {
	    	await parseParams(params, "YYYY-MM-DD")
        }
        catch (error) {
        	expect(error).to.be.a('string').that.contains('insufficient params') 
        	expect(error).to.contains('--help') 
        }
    })

    it('should return help when asked', async () => {
        let params = ['--help']
        try {
	    	await parseParams(params, "YYYY-MM-DD")
        }
        catch (error) {
        	expect(error).to.be.a('string').that.contains('Example') 
        	expect(error).to.not.contains('--help') 
        }
    })

    it('should return the params when correct', async () => {
        let params = ['United Kingdom']
	    const {countries, date} = await parseParams(params, "YYYY-MM-DD")
	    expect(countries).to.be.an('array').that.have.lengthOf(1)
	    expect(countries).to.include('United Kingdom')
    })

    it('should return the params with the first letter capitalized when correct', async () => {
        let params = ['united kingdom']
	    const {countries, date} = await parseParams(params, "YYYY-MM-DD")
	    expect(countries).to.be.an('array').that.have.lengthOf(1)
	    expect(countries).to.include('United kingdom')
    })
})