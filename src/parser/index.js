const {COMMAND} = require('../constants')

const usage = () => `
You must provide a list of country names separeted by spaces using quotation marks when needed.
$ ${COMMAND} country|"country" ... 

Example: 
$ ${COMMAND} "United Kingdom" Italy Germany`

const usageReminder = () => `
To see this help message again type: 
$ ${COMMAND} --help`

const capitalizeFirstLetter = (string) => string.charAt(0).toUpperCase() + string.slice(1)

const parseParams = (params, date) => {
    if (params.length < 1) {
	    return Promise.reject(`Error: insufficient params. \n${usage()}\n${usageReminder()}`)
    }

    if (params[0].toLowerCase().indexOf('--help') !== -1) {
        return Promise.reject(usage())
    }

    return Promise.resolve({
        countries: params.map(param => capitalizeFirstLetter(param)), 
        date
    })
}

module.exports = {
    parseParams
}