const cTable = require('console.table')

const compare = (a, b) => a > b ? 1 : (b < a ? -1 : 0)

const sortCountries = (countries) => 
    countries.sort((country1, country2) => 
        compare(parseInt(country2.population, 10), parseInt(country1.population, 10))
    )

const printCountries = (countries) => {
    let countryData = countries.map( ({country, population}, index) => [index+1, country, population])
  
    return cTable.getTable(['#', 'country', 'population'], countryData )
}

module.exports = {
    compare,
    sortCountries,
    printCountries
}