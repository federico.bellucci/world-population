const axios = require('axios')
const {API_BASE, COMMAND} = require('../constants')

const errorInvalid = (param) => `
Error: '${param}' is an invalid country. 

The list of valid values can be retrieved at ${API_BASE}/countries. 

Please check if you inserted the paramaters correctly with the command: 
$ ${COMMAND} --help`

const getCountries = ({countries, date}) => 
    Promise.all(countries.map(country => 
        axios.get(`${API_BASE}/${country}/${date}/`)
        	.then(response => ({
        		country,
        		population: response.data.total_population.population
        	}))
    ))
        .catch(error => {
            if (error.response.status === 400) {
                let re = /\s*(.+) is an invalid value/
                let match = re.exec(error.response.data.detail)
                if (match) {
                    return Promise.reject(errorInvalid(match[1]))
                }
            }
            return Promise.reject('Server error. Please try again later.')
        })

module.exports = {getCountries}