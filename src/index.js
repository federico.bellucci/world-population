const moment = require('moment')

const {getCountries} = require('./client')
const {sortCountries, printCountries} = require('./utils')
const {parseParams} = require('./parser')

let today = moment().format('YYYY-MM-DD')

parseParams(process.argv.slice(2), today)
    .then(getCountries)
    .then(sortCountries)
    .then(printCountries)
    .then(result => console.log('\n' + result))
    .catch(console.error)
